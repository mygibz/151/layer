﻿using System;

namespace Data
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Data:\tHello from the Data layer!");
        }
    }

    public class Person
    {
        // TODO: Change to private variables with custom getters and setters
        public string firstName;
        public string lastName;
        public int age;

        public Person(string firstName = "Joe", string lastName = "Doe", int age = 0)
        {
            this.firstName = firstName;
            this.lastName = lastName;
            this.age = age;

            Console.WriteLine("Data:\tAdded Person");
        }

    }
}

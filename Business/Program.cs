﻿using System;
using Data;
using System.Text.Json;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Business
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Business:\tHello from the Business layer!");

            Array.ForEach(Business.GetPersons(), LogPerson);
        }

        static void LogPerson(Person person)
        {
            Console.WriteLine($"Person {person.firstName} {person.lastName} is {person.age} years old");
        }
    }

    public class Business
    {
        public static Person[] GetPersons()
        {
            Person person1 = new Person("Isaac", "Arthur", 41);
            Person person2 = new Person("Albert", "Einstein", 76);

            Console.WriteLine("Business:\tGenerated array of all people");
            Person[] people = { person1, person2 };
            return people;
        }
        public static string GetPersonsJson()
        {
            Person[] people = GetPersons();

            //List<Person> output = new List<Person>();
            //foreach (Person person in people)
            //    output.Add(person);

            //string export = JsonConvert.SerializeObject(output);
            return JsonConvert.SerializeObject(GetPersons());
        }
    }
}
